from django.http import HttpResponseForbidden
from .forms import PublicContribForm


def prevent_robots(field_name='human_field'):
    """
    this decorator returns a HTTP 403 Forbidden error on POST requests
    if a given field has been set

    Keyword arguments :
    field_name -- the name of the field to search for (default 'human_field')
    """
    def _dec(func):
        def _wrapped_func(request, *args, **kwargs):
            if request.method == 'POST':
                form = PublicContribForm(request.POST)
                if field_name in form.data and form.data[field_name]:
                    return HttpResponseForbidden()
            return func(request, *args, **kwargs)
        return _wrapped_func
    return _dec
