from django import forms

from .models import Contrib


ORIENTATIONS = (
    ('N', 'Nord'),
    ('NO', 'Nord-Ouest'),
    ('O', 'Ouest'),
    ('SO', 'Sud-Ouest'),
    ('S', 'Sud'),
    ('SE', 'Sud-Est'),
    ('E', 'Est'),
    ('NE', 'Nord-Est'),
)


class PublicContribForm(forms.ModelForm):
    human_field = forms.CharField(required=False, widget=forms.HiddenInput)

    class Meta:
        model = Contrib

        fields = [
            'name', 'contrib_type',
            'latitude', 'longitude',
            'phone', 'email',
            'comment',
            'access_type',
            'floor', 'floor_total', 'orientations', 'roof',
            'comment',
            'privacy_name', 'privacy_email', 'privacy_coordinates',
            'privacy_place_details', 'privacy_comment',
        ]
        widgets = {
            'contrib_type': forms.RadioSelect,
            'latitude': forms.HiddenInput,
            'longitude': forms.HiddenInput,
            'access_type': forms.RadioSelect,
            'comment': forms.Textarea({'rows': 3}),
            'floor': forms.TextInput(
                attrs={'placeholder': "Étage (0 pour RDC)"}),
            'floor_total': forms.TextInput(
                attrs={'placeholder': "Nb. d'étages du bâtiment"}),
        }
    # Widget rendering is managed by hand in template for orientions.
    orientations = forms.MultipleChoiceField(choices=ORIENTATIONS)

    _privacy_fieldnames = (
        'privacy_name', 'privacy_email', 'privacy_coordinates',
        'privacy_place_details', 'privacy_comment',
    )

    def _validate_contact_information(self, data):
        if (data.get('phone') == '') and (data.get('email') == ''):
            msg = 'Il faut remplir un des deux champs "téléphone" ou "email".'
            self.add_error('phone', msg)
            self.add_error('email', msg)

    def _validate_floors(self, data):
        if None in (data.get('floor'), data.get('floor_total')):
            return

        if (data.get('floor') > data.get('floor_total')):
            self.add_error(
                'floor',
                "L'étage doit être inférieur ou égal au nombre d'étages",
            )

    def _validate_share_fields(self, data):
        if data.get('contrib_type') == Contrib.CONTRIB_SHARE:
            if data.get('access_type') == '':
                self.add_error('access_type', 'Ce champ est requis')

    def clean(self):
        cleaned_data = super().clean()
        self._validate_contact_information(cleaned_data)
        self._validate_floors(cleaned_data)
        self._validate_share_fields(cleaned_data)
        return cleaned_data

    def privacy_fields(self):
        for i in self._privacy_fieldnames:
            field = self[i]

            # FIXME: What a hack
            field.label = field.label\
                               .replace('public', '')\
                               .replace('publiques', '')
            yield field

    def __init__(self, *args, **kwargs):
        super(PublicContribForm, self).__init__(*args, **kwargs)

        for f in ['latitude', 'longitude']:
            self.fields[f].error_messages['required'] = "Veuillez déplacer le curseur à l'endroit où vous voulez partager/accéder au service"
